package com.dott.restaurants

import android.Manifest
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.dott.restaurants.data.RestaurantDto
import com.dott.restaurants.viewmodel.RestaurantsViewModel
import com.dott.restaurants.viewmodel.RestaurantsViewModelFactory
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.main_activity.*

class MainActivity : AppCompatActivity(), OnMapReadyCallback {

    companion object {
        const val LOCATION_PERMISSION = 1212
    }

    private lateinit var map: GoogleMap
    private lateinit var viewModel: RestaurantsViewModel
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        setupViewModel()
        checkLocationPermission()
    }

    private fun setupViewModel() {
        val factory = RestaurantsViewModelFactory.createFactory(this)
        viewModel = ViewModelProviders.of(this, factory).get(RestaurantsViewModel::class.java)
        viewModel.repository.restaurants.observe(this,
            Observer<List<RestaurantDto>> { restaurants ->
                if (restaurants.isNotEmpty())
                    updateMarkers(restaurants)
                else
                    clearMarkers()
            })
        viewModel.repository.exception.observe(this,
            Observer<Exception> { exception ->
                if (exception != null) {
                    Snackbar.make(main_view, "An error occurred, please try again.", Snackbar.LENGTH_LONG)
                        .setAction("RETRY") {
                            onMapCenterChanged()
                        }
                        .setActionTextColor(ContextCompat.getColor(this, android.R.color.white))
                        .show()
                }
            })
    }

    private fun checkLocationPermission() {
        val locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        if (locationPermission == PackageManager.PERMISSION_GRANTED)
            handleLocationEnabled()
        else
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION)
    }

    private fun handleLocationEnabled() {
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map_fragment) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.isMyLocationEnabled = true

        //Setup Listeners
        map.setOnCameraIdleListener {
            onMapCenterChanged()
        }
        map.setOnMarkerClickListener {
            updateView(viewModel.repository.getRestaurant(it.snippet))
            true
        }

        //Get Last Location and move Camera
        fusedLocationClient.lastLocation.addOnSuccessListener { location ->
            moveCamera(location)
        }
    }

    private fun onMapCenterChanged() {
        val currentCenter = map.cameraPosition.target
        val bounds = map.projection.visibleRegion.latLngBounds
        if (currentCenter != null && bounds != null)
            viewModel.mapMoved(currentCenter, bounds)
    }

    private fun updateView(restaurant: RestaurantDto?) {
        if (restaurant == null)
            return

        detail_view.visibility = View.VISIBLE

        restaurant_name.text = restaurant.name
        restaurant_country.text = restaurant.location.cc
        restaurant_address.text = restaurant.location.formattedAddress.toString()
    }

    private fun moveCamera(location: Location) {
        map.moveCamera(CameraUpdateFactory.newLatLng(LatLng(location.latitude, location.longitude)))
        map.moveCamera(CameraUpdateFactory.zoomTo(13.0f))
    }

    override fun onBackPressed() {
        if (!detail_view.isVisible)
            super.onBackPressed()
        else
            detail_view.visibility = View.GONE
    }

    private fun updateMarkers(restaurants: List<RestaurantDto>) {
        clearMarkers()
        for (restaurant in restaurants) {
            val location = LatLng(restaurant.location.lat, restaurant.location.lng)
            val marker = MarkerOptions().position(location).title(restaurant.name).snippet(restaurant.id)
            map.addMarker(marker)
        }
    }

    private fun clearMarkers() {
        map.clear()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == LOCATION_PERMISSION)
            if (permissions.size == 1 && permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                handleLocationEnabled()
            else
                showLocationErrorDialog()
    }

    private fun showLocationErrorDialog() {
        AlertDialog.Builder(this)
            .setTitle(getString(R.string.location_error))
            .setMessage(getString(R.string.location_error_msg))
            .setPositiveButton(android.R.string.ok) { dialog, _ ->
                checkLocationPermission()
                dialog.dismiss()
            }
            .setNegativeButton(android.R.string.no) { dialog, _ ->
                dialog.dismiss()
                finish()
            }
            .setIcon(android.R.drawable.ic_dialog_alert)
            .show()
    }
}
