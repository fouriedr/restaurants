package com.dott.restaurants.threading

import android.os.Handler
import android.os.Looper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class CoroutineExecutor : Executor {

    companion object {
        @JvmStatic
        private var instance: CoroutineExecutor? = null

        @JvmStatic
        fun getInstance(): CoroutineExecutor {
            synchronized(CoroutineExecutor::class.java) {
                if (instance == null)
                    this.instance = CoroutineExecutor()
            }
            return instance!!
        }
    }

    override fun executeInBackground(block: () -> Unit) {
        GlobalScope.launch(Dispatchers.IO) {
            block.invoke()
        }
    }

    override fun runOnUiThread(block: () -> Unit) {
        Handler(Looper.getMainLooper()).post {
            block.invoke()
        }
    }
}