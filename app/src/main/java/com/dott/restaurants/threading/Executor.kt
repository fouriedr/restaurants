package com.dott.restaurants.threading

interface Executor {

    fun executeInBackground(block: () -> Unit)

    fun runOnUiThread(block: () -> Unit)

}