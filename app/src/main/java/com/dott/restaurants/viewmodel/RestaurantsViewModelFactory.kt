package com.dott.restaurants.viewmodel

import android.app.Activity
import androidx.annotation.NonNull
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dott.restaurants.data.RestaurantsRepository
import com.dott.restaurants.threading.CoroutineExecutor

class RestaurantsViewModelFactory(private val repository: RestaurantsRepository) : ViewModelProvider.Factory {

    @NonNull
    override fun <T : ViewModel> create(@NonNull modelClass: Class<T>): T {
        try {
            return modelClass.getConstructor(RestaurantsRepository::class.java).newInstance(repository)
        } catch (e: Exception) {
            throw RuntimeException("Cannot create an instance of $modelClass", e)
        }
    }

    companion object {

        fun createFactory(activity: Activity): RestaurantsViewModelFactory {
            val application = activity.application ?: throw IllegalStateException("Not yet attached to Application")
            return RestaurantsViewModelFactory(RestaurantsRepository.getInstance(application, CoroutineExecutor.getInstance()))
        }

    }

}