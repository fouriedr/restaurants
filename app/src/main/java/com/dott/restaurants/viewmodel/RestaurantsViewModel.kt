package com.dott.restaurants.viewmodel

import androidx.lifecycle.ViewModel
import com.dott.restaurants.data.RestaurantsRepository
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds

class RestaurantsViewModel(var repository: RestaurantsRepository) : ViewModel() {

    fun mapMoved(latLong: LatLng, bounds: LatLngBounds) {
        repository.mapMoved(latLong, bounds)
    }
}