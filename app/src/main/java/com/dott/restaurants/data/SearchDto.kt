package com.dott.restaurants.data

import com.google.gson.annotations.SerializedName

data class SearchDto(
    @SerializedName("response") val response: Response
)

data class Response(
    @SerializedName("venues") val venues: List<RestaurantDto>
)