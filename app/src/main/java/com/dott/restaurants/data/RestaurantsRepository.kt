package com.dott.restaurants.data

import android.app.Application
import android.location.Location
import androidx.lifecycle.MutableLiveData
import com.dott.restaurants.api.ApiInterface
import com.dott.restaurants.api.Failure
import com.dott.restaurants.api.RestaurantsApi
import com.dott.restaurants.api.Success
import com.dott.restaurants.threading.Executor
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds


class RestaurantsRepository(private var api: ApiInterface, private var executor: Executor) {

    companion object {
        @JvmStatic
        private var instance: RestaurantsRepository? = null

        @JvmStatic
        fun getInstance(application: Application, executor: Executor): RestaurantsRepository {
            synchronized(RestaurantsRepository::class.java) {
                if (instance == null)
                    this.instance = RestaurantsRepository(RestaurantsApi.getInstance(application), executor)
            }
            return instance!!
        }
    }

    private val cachedRestaurants = mutableMapOf<String, RestaurantDto>()
    private lateinit var currentBounds: LatLngBounds
    private lateinit var latLng: LatLng

    val restaurants = MutableLiveData<List<RestaurantDto>>()
    val exception = MutableLiveData<Exception>()

    fun getRestaurant(restaurantId: String): RestaurantDto? {
        return this.cachedRestaurants[restaurantId]
    }

    fun mapMoved(latLng: LatLng, bounds: LatLngBounds) {
        this.latLng = latLng
        this.currentBounds = bounds

        executor.executeInBackground {
            updateRestaurantLiveData(true)
        }
    }

    private fun updateRestaurantLiveData(getFromServerWhenDone: Boolean) {
        val restaurantsInBounds = getRestaurantsInBounds()
        if (restaurantsInBounds.isNotEmpty())
            executor.runOnUiThread {
                restaurants.value = restaurantsInBounds
            }

        if (getFromServerWhenDone)
            getRestaurantsFromServer()
    }

    private fun getRestaurantsInBounds(): List<RestaurantDto> {
        val restaurantsInBounds = arrayListOf<RestaurantDto>()
        for (cachedRestaurant in cachedRestaurants.values) {
            val restaurantLatLng = LatLng(cachedRestaurant.location.lat, cachedRestaurant.location.lng)
            if (currentBounds.contains(restaurantLatLng))
                restaurantsInBounds.add(cachedRestaurant)
        }
        return restaurantsInBounds
    }

    private fun getRestaurantsFromServer() {
        val radius = getSearchRadius()
        when (val result = api.getRestaurantData("${latLng.latitude},${latLng.longitude}", radius)) {
            is Success -> {
                for (restaurant in result.value) {
                    if (!cachedRestaurants.containsKey(restaurant.id))
                        cachedRestaurants[restaurant.id] = restaurant
                }
                updateRestaurantLiveData(false)
            }
            is Failure -> {
                executor.runOnUiThread {
                    exception.value = result.reason
                }
            }
        }
    }

    private fun getSearchRadius(): Float {
        val centerLocation = Location("")
        centerLocation.latitude = latLng.latitude
        centerLocation.longitude = latLng.longitude

        val boundsLocation = Location("")
        boundsLocation.latitude = currentBounds.northeast.latitude
        boundsLocation.longitude = currentBounds.northeast.longitude

        return centerLocation.distanceTo(boundsLocation)
    }
}