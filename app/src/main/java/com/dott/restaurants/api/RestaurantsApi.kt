package com.dott.restaurants.api

import android.app.Application
import com.dott.restaurants.R
import com.dott.restaurants.data.RestaurantDto


class RestaurantsApi(private var clientId: String, private var clientSecret: String) : ApiInterface {

    companion object {
        const val API_VERSION = "20191011"
        const val API_INTENT = "checkin"
        const val API_FOOD_CATEGORY = "4d4b7105d754a06374d81259"

        @JvmStatic
        private var instance: RestaurantsApi? = null

        @JvmStatic
        fun getInstance(application: Application): RestaurantsApi {
            synchronized(RestaurantsApi::class.java) {
                if (instance == null) {
                    val clientId = application.getString(R.string.client_id)
                    val clientSecret = application.getString(R.string.client_secret)
                    this.instance = RestaurantsApi(clientId, clientSecret)
                }
            }
            return instance!!
        }
    }

    private var api: FourSquareApi = RetrofitService.createService(FourSquareApi::class.java)

    override fun getRestaurantData(latLong: String, radius: Float): Result<List<RestaurantDto>, Exception?> {
        var restaurants: List<RestaurantDto> = listOf()
        try {
            val response = api.getRestaurants(latLong, clientId, clientSecret, API_VERSION, API_INTENT, API_FOOD_CATEGORY, radius).execute()
            if (response.isSuccessful)
                restaurants = response.body()!!.response.venues
        } catch (ex: Exception) {
            ex.printStackTrace()
            return Failure(ex)
        }
        return Success(restaurants)
    }
}