package com.dott.restaurants.api

import com.dott.restaurants.data.RestaurantDto

interface ApiInterface {
    fun getRestaurantData(latLong: String, radius: Float): Result<List<RestaurantDto>, Exception?>
}
