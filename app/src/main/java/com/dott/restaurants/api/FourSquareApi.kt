package com.dott.restaurants.api

import com.dott.restaurants.data.SearchDto
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface FourSquareApi {

    @GET("venues/search")
    fun getRestaurants(
        @Query("ll") latLong: String, @Query("client_id") clientId: String,
        @Query("client_secret") clientSecret: String, @Query("v") version: String,
        @Query("intent") intent: String, @Query("categoryId") categoryId: String,
        @Query("radius") radius: Float
    ): Call<SearchDto>

}