package com.dott.restaurants.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.dott.restaurants.api.ApiInterface
import com.dott.restaurants.api.Success
import com.dott.restaurants.threading.CoroutineExecutor
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.gson.Gson
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito.doAnswer
import org.mockito.MockitoAnnotations

class RestaurantsRepositoryTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private lateinit var sut: RestaurantsRepository

    @Mock
    private lateinit var api: ApiInterface

    companion object {
        private const val capitalCraftJson = "{\n" +
                "        \"id\": \"57c6a4b0498efcc1c6c6dd81\",\n" +
                "        \"name\": \"Capital Craft Beer Academy\",\n" +
                "        \"location\": {\n" +
                "          \"address\": \"1 Logan Avenue\",\n" +
                "          \"crossStreet\": \"Bradford Street\",\n" +
                "          \"lat\": -25.87621981589704,\n" +
                "          \"lng\": 28.19785478844971,\n" +
                "          \"labeledLatLngs\": [\n" +
                "            {\n" +
                "              \"label\": \"display\",\n" +
                "              \"lat\": -25.87621981589704,\n" +
                "              \"lng\": 28.19785478844971\n" +
                "            }\n" +
                "          ],\n" +
                "          \"distance\": 16939,\n" +
                "          \"postalCode\": \"0157\",\n" +
                "          \"cc\": \"ZA\",\n" +
                "          \"neighborhood\": \"Highveld\",\n" +
                "          \"city\": \"Centurion\",\n" +
                "          \"state\": \"Gauteng\",\n" +
                "          \"country\": \"South Africa\",\n" +
                "          \"formattedAddress\": [\n" +
                "            \"1 Logan Avenue (Bradford Street)\",\n" +
                "            \"Centurion\",\n" +
                "            \"0157\",\n" +
                "            \"South Africa\"\n" +
                "          ]\n" +
                "        },\n" +
                "        \"categories\": [\n" +
                "          {\n" +
                "            \"id\": \"4bf58dd8d48988d1c4941735\",\n" +
                "            \"name\": \"Restaurant\",\n" +
                "            \"pluralName\": \"Restaurants\",\n" +
                "            \"shortName\": \"Restaurant\",\n" +
                "            \"icon\": {\n" +
                "              \"prefix\": \"https://ss3.4sqi.net/img/categories_v2/food/default_\",\n" +
                "              \"suffix\": \".png\"\n" +
                "            },\n" +
                "            \"primary\": true\n" +
                "          }\n" +
                "        ],\n" +
                "        \"referralId\": \"v-1570860272\",\n" +
                "        \"hasPerk\": false\n" +
                "      }"

        private const val wooliesFoodJson = "{\n" +
                "        \"id\": \"4daaf73c4df01c19b194e96f\",\n" +
                "        \"name\": \"Woolworths Food\",\n" +
                "        \"location\": {\n" +
                "          \"lat\": -25.818540198354608,\n" +
                "          \"lng\": 28.303731782060037,\n" +
                "          \"labeledLatLngs\": [\n" +
                "            {\n" +
                "              \"label\": \"display\",\n" +
                "              \"lat\": -25.818540198354608,\n" +
                "              \"lng\": 28.303731782060037\n" +
                "            }\n" +
                "          ],\n" +
                "          \"distance\": 4540,\n" +
                "          \"cc\": \"ZA\",\n" +
                "          \"city\": \"Parkview\",\n" +
                "          \"state\": \"Gauteng\",\n" +
                "          \"country\": \"South Africa\",\n" +
                "          \"formattedAddress\": [\n" +
                "            \"Parkview\",\n" +
                "            \"South Africa\"\n" +
                "          ]\n" +
                "        },\n" +
                "        \"categories\": [\n" +
                "          {\n" +
                "            \"id\": \"4bf58dd8d48988d118951735\",\n" +
                "            \"name\": \"Grocery Store\",\n" +
                "            \"pluralName\": \"Grocery Stores\",\n" +
                "            \"shortName\": \"Grocery Store\",\n" +
                "            \"icon\": {\n" +
                "              \"prefix\": \"https://ss3.4sqi.net/img/categories_v2/shops/food_grocery_\",\n" +
                "              \"suffix\": \".png\"\n" +
                "            },\n" +
                "            \"primary\": true\n" +
                "          }\n" +
                "        ],\n" +
                "        \"referralId\": \"v-1570860272\",\n" +
                "        \"hasPerk\": false\n" +
                "      }"
    }

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        sut = RestaurantsRepository(api, CoroutineExecutor())
    }

    @Test
    fun `initial restaurant data is an empty list`() {
        //when
        val data = sut.restaurants.value

        //then
        assert(data == null)
    }

    @Test
    fun `when the map moves and we have one restaurant, then return the list with one restaurant`() {
        //given
        val latLng = LatLng(-25.796882850326853, 28.3421241492033)
        val latLngString = "-25.796882850326853,28.3421241492033"
        val gson = Gson()

        val wooliesFood = gson.fromJson<RestaurantDto>(wooliesFoodJson, RestaurantDto::class.java)
        val restaurantList = arrayListOf<RestaurantDto>()
        restaurantList.add(wooliesFood)

        doAnswer { Success(restaurantList) }.`when`(api).getRestaurantData(latLngString, 1000f)

        val northEast = LatLng(-25.74721925864162, 28.37399449199438)
        val southWest = LatLng(-25.85460859422622, 28.303368240594864)
        val latLngBounds = LatLngBounds(southWest, northEast)

        //when
        runBlocking {
            sut.mapMoved(latLng, latLngBounds)
        }

        val returnList = arrayListOf<RestaurantDto>()
        returnList.add(wooliesFood)
        sut.restaurants.observeForever {
            assert(it == returnList)
        }
    }

    @Test
    fun `when the map moves and we have two restaurants, but only one is in the view bounds, then return the list with one restaurant`() {
        //given
        val latLng = LatLng(-25.796882850326853, 28.3421241492033)
        val latLngString = "-25.796882850326853,28.3421241492033"
        val gson = Gson()

        val capitalCraft = gson.fromJson<RestaurantDto>(capitalCraftJson, RestaurantDto::class.java)
        val wooliesFood = gson.fromJson<RestaurantDto>(wooliesFoodJson, RestaurantDto::class.java)

        val restaurantList = arrayListOf<RestaurantDto>()
        restaurantList.add(capitalCraft)
        restaurantList.add(wooliesFood)

        doAnswer { Success(restaurantList) }.`when`(api).getRestaurantData(latLngString, 1000f)

        val northEast = LatLng(-25.74721925864162, 28.37399449199438)
        val southWest = LatLng(-25.85460859422622, 28.303368240594864)
        val latLngBounds = LatLngBounds(southWest, northEast)

        //when
        runBlocking {
            sut.mapMoved(latLng, latLngBounds)
        }

        val returnList = arrayListOf<RestaurantDto>()
        returnList.add(wooliesFood)
        sut.restaurants.observeForever {
            assert(it == returnList)
        }
    }
}